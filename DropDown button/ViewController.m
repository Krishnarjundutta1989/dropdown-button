//
//  ViewController.m
//  DropDown button
//
//  Created by click labs 115 on 10/1/15.
//  Copyright (c) 2015 cli. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){
    NSArray *arrayDropDown;
    UITableViewCell *cell;
}
@property (strong, nonatomic) IBOutlet UITableView *tableViewDropDown;
@property (strong, nonatomic) IBOutlet UIButton *btnDropDown;

@end

@implementation ViewController
@synthesize tableViewDropDown;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrayDropDown = [NSArray new];
    arrayDropDown = @[@"SelectAnyOption :.....",@"prince",@"akshay",@"rohit",@"krish"];
    tableViewDropDown.hidden = TRUE;
    _btnDropDown.layer.borderWidth=1.0f;
    _btnDropDown.layer.borderColor=[[UIColor blackColor] CGColor];
    tableViewDropDown.layer.borderWidth = 1.0f;
    tableViewDropDown.layer.borderColor = [[UIColor blackColor]CGColor];
    // Do any additional setup after loading the view, typically from a nib.
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return  1;
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return arrayDropDown.count;
}
- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
cell = [tableView dequeueReusableCellWithIdentifier:@"reusedata"];
    cell.textLabel.text = arrayDropDown [indexPath.row];
   // cell.textLabel.textColor = [[[UIColor blueColor]CGColor];

    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView cellForRowAtIndexPath:indexPath];
    
    [_btnDropDown setTitle:[NSString stringWithFormat:@"%@",  cell.textLabel.text] forState:UIControlStateNormal];
    tableViewDropDown.hidden = TRUE;

}
- (IBAction)dropDown:(id)sender {
    
    tableViewDropDown.hidden = FALSE;
    [tableViewDropDown reloadData];
   
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
